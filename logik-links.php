<?php
/*
Plugin Name: List of Links
Plugin URI: 
Description: Displays links to anything you want
Version: 1.0
Author: Nicholas Fredricks
Author URI: http://www.logikgate.com
License: GPL2
*/

// Start class soup_widget //

class LINKERLIST_widget extends WP_Widget {

// Constructor //

	function LINKERLIST_widget() {
		$widget_ops = array( 'classname' => 'linkerlist_widget', 'description' => 'Displays links to anything you want' ); // Widget Settings
		$control_ops = array( 'id_base' => 'linkerlist_widget' ); // Widget Control Settings
		$this->WP_Widget( 'linkerlist_widget', 'List of Links', $widget_ops, $control_ops ); // Create the widget
	}
	// Extract Args //
	function widget($args, $instance) {
		extract( $args );
		$title 		 = apply_filters('widget_title', $instance['title']); // the widget title
		$links 		 = $instance['links']; // grab the links
		$link_titles = $instance['link_titles']; // the type of posts to show

		// Before widget //

			echo $before_widget;

	// Title of widget //

			if ( $title ) { echo $before_title . $title . $after_title; }

	// Widget output //
						?>
			<p>
			<?php 
				$links = preg_split('/\r\n|\r|\n/', $links);
				$link_titles = preg_split('/\r\n|\r|\n/', $link_titles);
				$link_count = count($links);
				$title_count = count($link_titles);
				if($link_count != $title_count){ ?>
					<p>The amount of links does not match the number of input link titles :o</p>
				<?php }
				else{ 
					echo "<p>";
					for($i = 0; $i < $link_count; $i++){ 
						if(strpos(trim($links[$i]), 'mailto:') === FALSE ){
							$before_link = "http://";
						}
						else{
							$before_link = "";
						}
					?>
						<a href="<?php echo $before_link.str_ireplace('http://', '', trim($links[$i])); ?>"><?php echo trim($link_titles[$i]); ?></a><br>
					<?php }
					echo "</p>";
				}
			?>

			<?php
			 ?>
			</p>

			<?php 

	// After widget //

			echo $after_widget;
		}




			// Update Settings //

 		function update($new_instance, $old_instance) {
 			$instance['title'] = strip_tags($new_instance['title']);
 			$instance['links'] = strip_tags($new_instance['links']);
 			$instance['link_titles'] = $new_instance['link_titles'];
 			$instance['author_credit'] = $new_instance['author_credit'];
 			return $instance;
 		}

 		// Widget Control Panel //

 		function form($instance) {

 		$defaults = array( 'title' => 'List of Links', 'links' => '', 'link_titles' => '', author_credit => 'on' );
 		$instance = wp_parse_args( (array) $instance, $defaults ); ?>

 		<p>
 			<label for="<?php echo $this->get_field_id('title'); ?>">Title:</label>
 			<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>'" type="text" value="<?php echo $instance['title']; ?>" />
 		</p>
 		 <p>
 			<label for="<?php echo $this->get_field_id('link_titles'); ?>">Link Titles (each on a new line):</label>
 			<textarea id="<?php echo $this->get_field_id('link_titles'); ?>" name="<?php echo $this->get_field_name('link_titles'); ?>" cols="30" rows="4"><?php echo $instance['link_titles']; ?></textarea>
 		</p>
 		<p>
 			<label for="<?php echo $this->get_field_id('links'); ?>">Website URLs (each on a new line):</label>
 			<textarea id="<?php echo $this->get_field_id('links'); ?>" name="<?php echo $this->get_field_name('links'); ?>" cols="30" rows="4"><?php echo $instance['links']; ?></textarea>
 		</p>
        <?php }

}

// End class soup_widget

add_action('widgets_init', create_function('', 'return register_widget("linkerlist_widget");'));
?>
